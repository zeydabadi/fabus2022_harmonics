#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Code to reproduce Figure 6 and related analyses of Fabus et al (2022).
Created on Fri Dec 17 11:51:43 2021

@author: MSFabus
"""
import numpy as np
from matplotlib import pyplot as plt
from neurons import FNNeuron
from scipy import signal, stats
from scipy.fft import fft, fftfreq
import emd

plt.rcParams['figure.dpi'] = 200
save = False

# Parameters
I_ampl = 0.475
tmax = 10000.
V_0 = -0.
W_0 = -0.4
Fs = 100
N = int(Fs*tmax)

# Create neuron and calculate harmonic properties
ts, t_step = np.linspace(0, tmax, N, retstep=True)
neuron = FNNeuron(I_ampl=I_ampl, V_0=V_0, W_0=W_0)
neuron.solve(ts=ts)
Vs = neuron.Vs

freqs, psd = signal.welch(Vs, fs=100*1000, nperseg=100000)

Fs = 100*1000
yf = fft(Vs, )
xf = fftfreq(N, 1/Fs)[:N//2]
yf_real = 2.0/N * np.abs(yf[0:N//2])

peaks, props = signal.find_peaks(yf_real, distance=1, height=0.0015)
y_peaks = np.array([yf_real[x] for x in peaks[:30]])
f_peaks = np.array([xf[x] for x in peaks[:30]])

# Fit 1/x^gamma model
slope, intercept, r, p, se = stats.linregress(np.log10(f_peaks), np.log10(y_peaks))

freq = np.linspace(20, 1270, 100)
ypred_oof = 10**(intercept + slope*np.log10(freq))

# Find IF
IP, IF, IA = emd.spectra.frequency_transform(Vs, Fs, 'hilbert')

#%% Plotting
fig, (ax2, ax3, ax) = plt.subplots(1, 3, figsize=(7.0625, 2), 
                              gridspec_kw={'width_ratios': [2., 2, 2.]},)

ax2.annotate('A', xy=(-70, 2), fontsize=20, annotation_clip=False)
ax2.annotate('B', xy=(220, 2), fontsize=20, annotation_clip=False)
ax2.annotate('C', xy=(550, 2), fontsize=20, annotation_clip=False)

ax.loglog(xf, yf_real , c='k', lw=1)
ax.scatter(f_peaks, y_peaks, s=15, color='red', label='Harmonic\npeaks')
ax.loglog(freq, ypred_oof, c='b', label='$1/f^{\gamma}$ fit')

ax.set_xlim(20, 860)
ax.set_ylim(5e-4, 3)
ax.set_xticks([25, 100, 400,])
ax.set_xticklabels(['25', '100', '400',])
ax.legend(fontsize=8, ncol=2, bbox_to_anchor=(1.2, 1.3), loc='upper right')
ax.minorticks_off()
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('Power Spectrum', labelpad=2)

ax2.plot(ts, neuron.Vs, lw=1, c='k')
ax2.set_xlim(0, 200)
ax2.set_ylabel('Signal', labelpad=3)
ax2.set_xlabel('Time [ms]')


ax3.plot(ts[2:], IF[2:, 0], lw=1, c='k')
ax3.set_xlim(0, 200)
ax3.set_ylim(0, 100)
ax3.set_xlabel('Time [ms]')
ax3.set_ylabel('IF [Hz]', labelpad=3)

plt.subplots_adjust(wspace=0.6, hspace=0.45)

if save:
    fname = 'fig_FN.png'
    fig.savefig(fname, format='png', bbox_inches = 'tight', dpi=600)
plt.show()