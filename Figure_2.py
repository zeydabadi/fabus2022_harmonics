#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Code to reproduce Figure 2 and related analyses of Fabus et al (2022).
Created on Fri Dec 17 11:51:43 2021

@author: MSFabus
"""
import matplotlib.pyplot as plt
import numpy as np
import emd
from matplotlib.patches import ConnectionPatch

plt.rcParams['figure.dpi'] = 200
save = False
mode = 'pre-comp'

seconds = 120
sample_rate = 1024
time_vect = np.linspace(0,seconds,seconds*sample_rate)

a = 10**(np.linspace(-2,2))
f = np.linspace(.01,1,60)
nphases = 10
p = np.linspace(0,2*np.pi,nphases+1)[:-1]

if mode == 'pre-comp':
    # Use pre-computed data
    dat = np.load('TwoTone_Data.npz')
    nextr = dat['nextr']
    metric = dat['metric2']

else:
    # Calculate from scratch
    metric = np.zeros((len(f),len(a),len(p)))
    nextr =  np.zeros_like(metric)
    imf_opts = {'stop_method':'sd', 'energy_thresh': 30}
    for ii in range(len(f)):
        print(ii, end=' ')
        for jj in range(len(a)):
            for kk in range(len(p)):
    
                HF = np.cos(2*np.pi*time_vect)
                LF = a[jj]*np.cos(2*np.pi*f[ii]*time_vect+p[kk])
    
                imf = emd.sift.sift(HF+LF, imf_opts=imf_opts, verbose='WARNING', max_imfs=1)
                metric[ii,jj,kk] = np.corrcoef(HF,imf[:,0])[0,1]
    
                locs,pks = emd.sift._find_extrema(HF+LF)
                nextr[ii,jj,kk] = len(locs)

af = 1/a
af2 = 1/np.sqrt(a)
ones = np.ones(len(a))

# Plotting
fig, ax1 = plt.subplots(figsize=(3.5, 3.5))
ax1.pcolormesh(-np.log10(a), f, metric.mean(axis=2), cmap='Greys', vmin=0,
               vmax=1.5, shading='gouraud')
ax1.contour(-np.log10(a), f, metric.mean(axis=2), [0.5], colors='black', linewidths=1)
ax1.plot(-np.log10(a), af, c='red')
ax1.plot(-np.log10(a), af2, c='red', linestyle='--')


[ax1.axhline(i, alpha=0.5, color='purple', lw=1, zorder=1) for i in [1/f for f in range(2, 100)]]
ax1.set_ylabel('$1/f$')
ax1.set_xlabel('$log_{10}(a)$')
ax1.set_ylim(0, 1)

# Add insert axes for example plot
t = np.linspace(0, 3, 300)
x1 = np.cos(2*np.pi*t) + 0.2*np.cos(2*np.pi*2*t+np.pi)
left, bottom, width, height = [0.175, 0.6, 0.2, 0.2]
ax2 = fig.add_axes([left, bottom, width, height])
ax2.plot(t, x1, c='midnightblue')
ax2.set_xticks([])
ax2.set_yticks([])
ax2.annotate('i', xy=(2.85, -1.2), fontsize=7, annotation_clip=False)
ax1.scatter([np.log10(0.2)], [1/2], alpha=1, s=10, c='darkorange', zorder=2)
ax1.plot([np.log10(0.2), -1.25], [1/2, 0.67], c='darkorange')

x2 = np.cos(2*np.pi*t) + 0.33*np.cos(2*np.pi*2*t+np.pi)
left, bottom, width, height = [0.6, 0.6, 0.2, 0.2]
ax2 = fig.add_axes([left, bottom, width, height])
ax2.plot(t, x2, c='midnightblue')
ax2.set_xticks([])
ax2.set_yticks([])
ax2.annotate('ii', xy=(2.75, -1.3), fontsize=7, annotation_clip=False)
ax1.scatter([np.log10(0.33)], [1/2], alpha=1, s=10, c='darkorange', zorder=2)
ax1.plot([np.log10(0.33), 0.8], [1/2, 0.67], c='darkorange')

x3 = np.cos(2*np.pi*t) + 2*np.cos(2*np.pi*2*t+np.pi)
left, bottom, width, height = [0.6, 0.2, 0.2, 0.2]
ax2 = fig.add_axes([left, bottom, width, height])
ax2.plot(t, x3, c='midnightblue')
ax2.set_xticks([])
ax2.set_yticks([])
ax2.annotate('iii', xy=(2.66, -3.), fontsize=7, annotation_clip=False)
ax1.scatter([np.log10(2)], [1/2], alpha=1, s=10, c='darkorange', zorder=2)
ax1.plot([np.log10(2), 1.25], [1/2, 0.3], c='darkorange')

if save:
    fname = 'fig2.png'
    fig.savefig(fname, format='png', bbox_inches = 'tight', dpi=600)
plt.show()