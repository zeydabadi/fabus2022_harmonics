#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Code to reproduce Figures 1, 3, 5, 8 of Fabus et al (2022).
Created on Fri Dec 17 11:51:43 2021

@author: MSFabus
"""
import emd
import  numpy as np
from scipy import signal, stats
from scipy.fft import fft, fftfreq
import matplotlib.pyplot as plt
from matplotlib.patches import ConnectionPatch

plt.rcParams['figure.dpi'] = 200

def get_axis_limits(ax, scale=.9):
    return ax.get_xlim()[1]*(-0.03), ax.get_ylim()[1]*1.05

save = False
fail
#%% FIGURE 1

def IF(x, a, f):
    num = f * (1 + 2*a**2 + 3*a*np.cos(20*np.pi*x + np.pi/2))
    den = 1 + a**2 + 2*a*np.cos(20*np.pi*x + np.pi/2)
    return num / den

seconds = 10
sample_rate = 1000
t = np.linspace(0, seconds, seconds*sample_rate)
t2 = t[:100]*1000
# First we create a slow 10Hz oscillation and a fast 20Hz oscillation
slow = np.sin(2*np.pi*10*t)
fast = np.cos(2*np.pi*20*t)

y = np.cos(2*np.pi*10*t)
f = 0.5

amps = [0.2, 0.75]
labs = ['A', 'B', 'C', 'D']
xlabs = ['', '', 'Time [ms]', 'Time[ms]']
ylabs = ['Signal', '', 'IF [Hz]', '']

plt.rcParams['figure.dpi'] = 400
plt.rc('font', size=12)
fig, axs = plt.subplots(2, 2, figsize=(3.5, 3.))
for i in [0, 1]:
    
    # We create our signal by summing the oscillation and adding some noise
    x0am = slow + fast * amps[i] 
    
    # Quick summary figure
    ax = axs.flatten()[i]
    ax.plot(t2, y[175:275], c='k', lw=2)
    ax.plot(t2, x0am[100:200], color='red', lw=2)
    ax.annotate(labs[i], xy=get_axis_limits(ax), fontsize=20, annotation_clip=False)
    ax.set_ylabel(ylabs[i])
    ax.yaxis.set_label_coords(-0.31,0.6)
    for d in ["left", "top", "bottom", "right"]:
        ax.spines[d].set_visible(False)
    ax.set_yticks([])
    ax.set_xticks([])
    
    # Get IF
    baseF = np.array([10 for _ in t])
    zero = np.array([0 for _ in t])
    instF = IF(t, amps[i], 10)
    ax = axs.flatten()[i+2]
    ax.plot(t2, baseF[175:275], c='k', lw=2)
    ax.plot(t2, zero[175:275], c='k', lw=2)
    ax.plot(t2, instF[100:200], color='blue', lw=2)
    if i == 0:
        ax.annotate(labs[i+2], xy=(-5, 16.7), fontsize=20, annotation_clip=False)
    else:
            ax.annotate(labs[i+2], xy=get_axis_limits(ax), fontsize=20, annotation_clip=False)
    ax.set_ylim([-5, 15])
    ax.set_ylabel(ylabs[i+2])
    ax.set_xlabel(xlabs[i+2])
    for d in ["left", "top", "bottom", "right"]:
        ax.spines[d].set_visible(False)
        
if save:
    fname = 'fig1.png'
    fig.savefig(fname, format='png', bbox_inches = 'tight', dpi=600)
plt.show()


#%% FIGURE 3

seconds = 12.56
sample_rate = 1000
t = np.linspace(0, seconds, int(seconds*sample_rate))

# First we create a slow 10Hz oscillation and a fast 20Hz oscillation
slow = np.sin(2*np.pi*10*t)
fast = np.cos(2*np.pi*20*t)

fast_am = 0.5*slow + 0.5
y = np.cos(2*np.pi*10*t)

g = [1, 2]
cols = ['r', 'b']
labs = ['A', 'B']
plt.rcParams['figure.dpi'] = 400
fig, axs = plt.subplots(1, 2, figsize=(3.5, 1.5))
for i in [0, 1]:
    y = np.sum([1/n**g[i] * np.sin(n*t) for n in range(1, 20)], 0)
    ax = axs[i]
    ax.plot(y, c=cols[i], lw=1.5)
    ax.annotate(labs[i], xy=get_axis_limits(ax), fontsize=20, annotation_clip=False)
    ax.axis('off')
 
if save:
    fname = 'fig3.png'
    fig.savefig(fname, format='png', bbox_inches = 'tight', dpi=600)
plt.show()


#%% INSETS FOR FIGURE 4

seconds = 10
sample_rate = 1000
t = np.linspace(-5, seconds+5, int((seconds+10)*sample_rate))
Ns = len(t)

# First we create a slow 10Hz oscillation and a fast 20Hz oscillation
slow = np.sin(2*np.pi*10*t)
fast = np.cos(2*np.pi*20*t)

fast_am = 0.5*slow + 0.5
y = np.cos(2*np.pi*10*t)

g = [1.1, 2.5, 1]
ps = [2.5, 2.5, 0]
cols = ['r', 'b']
plt.rcParams['figure.dpi'] = 400
for i in [0, 1, 2]:
    fig = plt.figure(figsize=(3.5, 1.5))
    ax = plt.subplot2grid((2, 2), (0, 0), colspan=1, rowspan=1)
    ax2 = plt.subplot2grid((2, 2), (1, 0), colspan=1, rowspan=1)
    ax3 = plt.subplot2grid((2, 2), (0, 1), colspan=1, rowspan=2)
    plt.subplots_adjust(wspace=0.7)
    
    y = np.sum([1/(n)**g[i] * np.sin(2*np.pi*n*t+ps[i]) for n in range(1, 8)], 0)
    
    if i==2:
        y = np.sum([1/(n)**g[i] * np.sin(2*np.pi*n*t) for n in [1,  3.33, 5.2, 6.7]], 0)
        y += 0.3*np.sin(2*np.pi*5.2*t)
        
    IP,IF,IA = emd.spectra.frequency_transform(y, sample_rate, 'hilbert')

    ax.plot(t, y, c='k', lw=1.)
    ax.set_xlim(-0.1, 4*np.pi+0.1)
    for d in ["top", "right"]:
        ax.spines[d].set_visible(False)
    ax.set_xticks([])
    ax.set_ylabel('Signal')
    ax.set_xlim(5, 8)
    
    ax2.plot(t, IF, lw=1, c='k')
    ax2.set_ylim(-0.5, 3)
    ax2.set_xlim(5, 8)
    ax2.set_yticks([0, 1, 2])
    ax2.set_xticklabels(['0', '1', '2'])
    for d in ["top", "right"]:
        ax2.spines[d].set_visible(False)
    ax2.set_xlabel('Time [s]')
    ax2.set_ylabel('IF [Hz]', labelpad=12)
        
    # Find FFT and fit spectral peaks
    yf = 2/Ns * np.abs(fft(y))[:Ns//2]
    xf = fftfreq(Ns, 1/sample_rate)[:Ns//2]
    yf[yf<1e-3] = 0
    peaks, props = signal.find_peaks(yf, distance=1, height=0.0015)
    y_peaks = np.array([yf[x] for x in peaks[:30]])
    f_peaks = np.array([xf[x] for x in peaks[:30]])

    # Fit 1/x^gamma model
    slope2, intercept2, r2, p2, se2 = stats.linregress(np.log10(f_peaks), np.log10(y_peaks))
    
    freq = np.linspace(0, 10, 100)
    ypred_oof = 10**(intercept2 + slope2*np.log10(freq))
    
    ax3.semilogy(xf, yf, c='k')
    
    if i != 2:
        ax3.semilogy(freq, ypred_oof, c='grey', label='$1/f^{\gamma}$ fit')
        ax3.legend(fontsize=6, bbox_to_anchor=(1., 1.), loc='upper right')

    ax3.set_xlim(0, 8)
    ax3.set_ylim(1e-3, 1.5)
    ax3.set_xlabel('Frequency [Hz]')
    ax3.set_ylabel('Power spectrum')
    ax3.set_xticks([0, 2, 4, 6, 8])
    plt.show()
    

#%% FIGURE 5

def a(n, gamma):
    return 1 / n**gamma

def f(n):
    return n

def general_cos_sum(t, a_fn, f_fn, phi, Nharm, **kwargs):
    x = np.zeros(len(t))
    for i in range(1, Nharm+1):
        x += a_fn(i, kwargs['gamma']) * np.cos(f_fn(i)*t - phi)
    return x

def Compute_IA(t, a_fn, f_fn, phi, Nharm, **kwargs):
    cos = general_cos_sum(t, a_fn, f_fn, phi, Nharm, **kwargs)
    sin = general_cos_sum(t, a_fn, f_fn, phi+np.pi/2, Nharm, **kwargs)
    IA = np.sqrt(cos**2 + sin**2)
    return IA
    
def Compute_IF(t, k, Nharm, phi, IA):
    ns = np.arange(1, Nharm+1, 1).astype(float)
    ms = np.arange(1, Nharm+1, 1).astype(float)
    ngrid, xgrid, mgrid = np.meshgrid(ns, t, ms)
    cos = 1/ngrid**k * 1/mgrid**(k-1) * np.cos((ngrid-mgrid) * xgrid)
    IF_curve = 1/(2*np.pi) * np.sum(cos[:, :Nharm, :Nharm], (1, 2)) / IA**2
    return IF_curve

fs = 200
Ns = 6*np.pi
Nharm = 4
phi = 0
t = np.linspace(0, Ns, int(Ns*fs))
labs = ['A', 'B']
labs2 = ['$\gamma=1.25$', '$\gamma=2.25$']
plt.rc('font', size=14)
fig = plt.figure(figsize=(7.0625, 3.5))
for j, gamma in enumerate([1.25, 2.25]):

    ax = plt.subplot2grid((2, 3), (j, 0), colspan=1, rowspan=1)
    ax2 = plt.subplot2grid((2, 3), (j, 1), colspan=1, rowspan=1)
    
    x_cos = general_cos_sum(t, a, f, phi, Nharm, gamma=gamma) # Main cosine sum
    
    f0 = 2*np.pi*1
    IA_analytic = Compute_IA(t, a, f, phi, Nharm, gamma=gamma)
    IF_analytic = Compute_IF(t, gamma, Nharm, phi, IA_analytic)
    
    # Plot circular figure
    ph = 2*np.pi*np.cumsum(IF_analytic) * 1/fs
    y = IA_analytic * np.cos(ph)
    x = IA_analytic * np.sin(ph)
    
    i = int(5*len(t) / 6)
    xy = (x[i],y[i])
    ax2.plot(x, y, zorder=1, lw=2, c='blue')
    ax2.scatter(x[i], y[i], c='k', zorder=2, s=10)
    ax2.set_yticks([])
    ax2.set_xticks([])
    
    ax.plot(t, x_cos, alpha=1, zorder=1, lw=2, c='blue')
    ax.scatter(t[i], x_cos[i], c='k', zorder=2, s=10)
    ax.set_yticks([])
    ax.set_xticks([])
    ax.annotate(labs[j], xy=(-4.5, 1), fontsize=25, annotation_clip=False)
    
    if j == 0:
        ax.annotate('Timeseries plot', xy=(-1.5, 2.25), fontsize=16, annotation_clip=False)
        ax2.annotate('Polar plot', xy=(-0.9, 2.25), fontsize=16, annotation_clip=False)
        ax2.annotate(labs2[j], xy=(-0.6, 0.45), fontsize=12, annotation_clip=False)
    if j == 1:
        ax2.annotate(labs2[j], xy=(-0.45, 0.2), fontsize=12, annotation_clip=False)
    
    ty = (t[i], x_cos[i])
    con = ConnectionPatch(xyA=ty, xyB=xy, coordsA="data", coordsB="data",
                          axesA=ax, axesB=ax2, color="red", lw=2)
    ax2.add_artist(con)
    for d in ["left", "top", "bottom", "right"]:
        ax.spines[d].set_visible(False)
        ax2.spines[d].set_visible(False)
        

# Plot Dirichlet eta
ax3 = plt.subplot2grid((2, 3), (0, 2), colspan=1, rowspan=2)
gs = np.linspace(1.0, 4, 50)

for i, N in enumerate([2, 10, 50]):
    eta = np.sum([[(-1)**(n+1) / n**(g-2) for n in range(1, N+1)] for g in gs], 1)
    ax3.plot(gs, eta, lw=2, label='N='+str(N))
ax3.grid(True)
ax3.legend(fontsize=12)
ax3.set_xlim([1, 4])
ax3.set_ylim([-8, 1])
ax3.set_title('$\ddot{x}(\pi)$')
ax3.annotate('C', xy=(0.33, 1), fontsize=25, annotation_clip=False)
ax3.set_xlabel('$\gamma$', labelpad=-5)

if save:
    fname = 'fig5.png'
    fig.savefig(fname, format='png', bbox_inches = 'tight', dpi=600)
plt.show()


#%% FIGURE 8

def a1(n):
    return 1 / (2*n+1)**2
def f1(n):
    return 2*n+1

def a2(n):
    return 1 / n
def f2(n):
    return n

def a3(n):
    return 1 / (2*n+1)
def f3(n):
    return 2*n+1

def general_cos_sum(t, a_fn, f_fn, phi, Nharm, n0=1, **kwargs):
    x = np.zeros(len(t))
    for i in range(n0, Nharm+1):
        x += a_fn(i) * np.cos(f_fn(i)*t - phi)
    return x

fs = 200
Ns = 4*np.pi+0.1
t = np.linspace(-0.1, Ns, int(Ns*fs))
y1 = general_cos_sum(t, a1, f1, 0, 1000, n0=0)
y2 = general_cos_sum(t, a2, f2, np.pi/2, 1000)
y3 = general_cos_sum(t, a3, f3, np.pi/2, 1000, n0=0)
ys = [y1, y2, y3]

IP1, IF1, IA1 = emd.spectra.frequency_transform(y1, fs, 'hilbert' )
IP1, IF2, IA1 = emd.spectra.frequency_transform(y2, fs, 'hilbert' )
IP1, IF3, IA1 = emd.spectra.frequency_transform(y3, fs, 'hilbert' )
IFs = [IF1, IF2, IF3]

baseF = np.array([0.5 for _ in t])
zero = np.array([0 for _ in t])
labs = ['A', 'B', 'C', 'D']
    
lws = [2, 2, 2]
fig, axs = plt.subplots(2, 3, figsize=(7.0625, 3.5))
plt.subplots_adjust(wspace=0.33, hspace=-0.01)
for i in range(3):
    ax1 = axs[0, i]
    ax2 = axs[1, i]
    ax1.plot(t[20:-20], ys[i][20:-20], lw=2, c='red')
    ax1.set_xlim(-0.1, 4*np.pi+0.1)
    ax1.set_yticks([])
    ax1.set_xticks([])
    ax1.annotate(labs[i], xy=[-0.2, 1], fontsize=20, annotation_clip=False, xycoords='axes fraction')
    
    ax2.plot(t[20:-20], IFs[i][20:-20], lw=lws[i], c='blue')
    ax2.plot(t, zero, c='k', lw=2)
    ax2.set_ylim(-0.01, 0.75)
    ax2.set_xticks([0, 2*np.pi, 4*np.pi])
    ax2.set_xticklabels(['0', '$2\pi$', '$4\pi$'])
    ax2.set_xlim(-0.1, 4*np.pi+0.1)
    ax2.set_yticks([0, 0.5])
    ax2.set_yticklabels(['0', '0.5'])
    
    for d in ["left", "top", "bottom", "right"]:
        ax2.spines[d].set_visible(False)
        ax1.spines[d].set_visible(False)
        
axs[1, 0].set_ylabel('IF [Hz]')
axs[0, 0].set_ylabel('Signal', labelpad=35)

if save:
    fname = 'fig_8.png'
    fig.savefig(fname, format='png', bbox_inches = 'tight', dpi=600)
plt.show()