#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 17:55:00 2021

@author: MSFabus
"""

import dash
from dash.dependencies import Input, Output, State
import plotly.express as px
import pandas as pd
import emd
import numpy as np
from scipy.fft import fft, fftfreq
from scipy import stats
import dash_bootstrap_components as dbc
from dash import html, dcc

def card(header, name, props, traffic_light=''):
    card_content = [
        dbc.CardBody(
            [
                html.H4(header, className="card-title",id=header),
                html.Div(
                    style={'width':'90%'},
                            children =   [
                                
                                dbc.Row(
                                    [
                                        
                                    dbc.Col(
                                        html.Div(
                                            children=[html.P('Base'), 
                                                      html.P('Harmonic 1'), 
                                                      html.P('Harmonic 2')]
                                            ),
                                        width=4
                                    ),
                                    
                                    dbc.Col(
                                    [
                                        html.Div(
                                            style={'padding-right':'0%', 'width':'120%'}, 
                                            children=[dcc.Slider(
                                                id=name[0],
                                                min=props[0],
                                                max=props[1],
                                                value=props[2][0],
                                                step=props[3],
                                                marks=props[4],
                                                ), 
                                         dcc.Slider(
                                                id=name[1],
                                                min=props[0],
                                                max=props[1],
                                                value=props[2][1],
                                                step=props[3],
                                                marks=props[4]
                                                ), 
                                          dcc.Slider(
                                                id=name[2],
                                                min=props[0],
                                                max=props[1],
                                                value=props[2][2],
                                                step=props[3],
                                                marks=props[4],
                                                ),])
                                          
                                     ],
                                        width=8)
                                ])
                                         ]
                                )
            ]
                    )       
                ]   

    return card_content


switches = html.Div(
    [
        dbc.Label(""),
        dbc.Switch(
            id="advanced-switch",
            label="Advanced view",
            value=False,
        ),
        
    ]
)

#%% Example plotly figure
Ns = 800 
Tmax = 3
ts = np.linspace(-0.5, Tmax+0.5, Ns)
Fs = Ns/(Tmax+1)

headers = ['Amplitude [mV]', 'Frequency [Hz]', 'Phase [degrees]']   
names = [str(i) for i in range(9)]    

marks_amp = {str(x): {'label':str(round(x, 2)), 'style':{'color':'black'}} for x in np.linspace(0, 1, 6)}
marks_f = {str(int(x)): {'label':str(round(x)), 'style':{'color':'black'}} for x in np.linspace(0, 5, 6)}
marks_phi = {str(x): {'label':str(round(x*180/np.pi)), 'style':{'color':'black'}} for x in np.linspace(0, 2*np.pi, 5)}

props_amp = [0, 1, [1, 0.2, 0], 0.05, marks_amp]
props_f = [0, 5, [1, 2, 3], 0.5, marks_f]
props_phi = [0, 2*np.pi, [0, 0, 0], np.pi/4, marks_phi] 
#%%

instructions = html.Div(
    [
        dbc.Button("Instructions", id="open-fs", size="lg"),
        dbc.Modal(
            [
                dbc.ModalHeader(dbc.ModalTitle("Instructions")),
                dbc.ModalBody(
                    html.Div([
                        html.A("Accompanying publication", 
                           href='https://www.biorxiv.org/content/10.1101/2021.12.21.473676v1', target="_blank"),
                        html.H5(""),
                        html.P(
                    "This simulator generates non-sinusoidal waveforms from\
                    three user-defined sinusoids. The user can control the \
                    amplitude, frequency, and phase of each sinusoid \
                    by the corresponsing slider. The output graphs show \
                    the waveform, its instantaneous frequency, and its spectrum."),
                    html.H5(" "),
                    html.P("\
                    The 'Harmonic conditions met' traffic light shows whether\
                    the resulting waveform should (green) or should not (red) be considered a harmonic\
                    structure according to 2 conditions from Fabus et al. (2022).\
                    These conditions are (1) integer frequency ratios and constant phase\
                    and (2) non-negative instantaneous frequency. The user is\
                    encouraged to explore how clear secondary oscillatory\
                    dynamics is present when these are not met."),
                    html.H5(" "),
                    html.P("\
                    Additionally, in 'Advanced mode', each condition gets\
                    its own traffic light and the user can see which of \
                    amplitude, frequency, and phase is driving the results.\
                    A 1/f^gamma fit is also added to the spectrum, so\
                    the user can explore differences between strong (gamma>2)\
                    and weak (gamma<=2) harmonics."),
                    ])),
            ],
            id="modal-fs",
            fullscreen=False,
            scrollable=True,
            size="lg",
        ),
    ]
)

app = dash.Dash(__name__, title='Harmonics', external_stylesheets=[dbc.themes.BOOTSTRAP])
app.layout = html.Div(
    [
        dcc.Location(id='url', refresh=False),     
        html.H1('Waveform Shape Simulator'),
        
        dbc.Row(
            [
                dbc.Col(dbc.Card(card(headers[0], names[:3], props_amp), style={"width": "20vw"}, 
                                 color="light", inverse=False),
                        width=3, id='amp-col'),
                dbc.Col(dbc.Card(card(headers[1], names[3:6], props_f), style={"width": "20vw"}, color="light", inverse=False),
                        width=3),
                dbc.Col(dbc.Card(card(headers[2], names[6:9], props_phi), style={"width": "20vw"}, color="light", inverse=False),
                        width=3),
                dbc.Col(children=[instructions, switches])
            ],
        ),
        
        dbc.Row(
            [
            dbc.Col(
                [
                dbc.Row(dcc.Graph(id='graph-sig', style={'width': '60vw', 'height': '35vh'})),
                dbc.Row(dcc.Graph(id='graph-if', style={'width': '60vw', 'height': '35vh'})),
                ]
                ),
            dbc.Col(
                [
                dbc.Row([dcc.Graph(id='graph-fft', style={'width': '35vw', 'height': '70vh'})])
                ]
                )
            ]
            )
    ]
)


@app.callback(
    Output("modal-fs", "is_open"),
    Input("open-fs", "n_clicks"),
    State("modal-fs", "is_open"),
)
def toggle_modal(n, is_open):
    if n:
        return not is_open
    return is_open


@app.callback(
    [Output('graph-sig', 'figure'),
     Output('graph-if', 'figure'),
     Output('graph-fft', 'figure'),
     Output(headers[0], 'children'),
     Output(headers[1], 'children'),
     Output(headers[2], 'children')],
    [Input(component_id='advanced-switch', component_property='value')]+
    [Input(x, 'value') for x in names],)
def update_figure(light, amp1, amp2, amp3, f1, f2, f3, p1, p2, p3):
    
    ## Set up signal, IF, and FFT
    y1 = amp1*np.sin(2*np.pi*f1*ts + p1)
    y2 = amp2*np.sin(2*np.pi*f2*ts + p2)
    y3 = amp3*np.sin(2*np.pi*f3*ts + p3)
    y = y1 + y2 + y3
        
    # Compute IF
    IP, IF, IA = emd.spectra.frequency_transform(y, Fs, 'hilbert')
    IF = np.squeeze(IF)
    IF[:3] = np.nan
    IF[-3:] = np.nan
    IFmax = np.nanpercentile(IF, 99.9)
    
    # Compute FFT
    yf = 2/Ns * np.abs(fft(y))[:Ns//2]
    xf = fftfreq(Ns, 1/Fs)[:Ns//2]
        
    df = pd.DataFrame(dict(
        t=ts,
        y=y,
        IF=IF,
        zeros=[0 for x in IF],
    ))
    
    df2 = pd.DataFrame(dict(
        yf=yf,
        xf=xf
    ))
    
    ## Check if harmonic conditions passed
    passed = True
    conds = [True, True, True]
    
    fs = np.array([f1, f2, f3])
    amps = np.array([amp1, amp2, amp3])
    baseF = np.min(fs)
    ratios = np.array([f1%baseF, f2%baseF, f3%baseF])
    active =  amps > 0
    
    # Check integer frequency ratios
    for ix, val in enumerate(active):
        if val and ratios[ix] != 0:
            passed = False
            conds[1] = False
            
    # Check not a sub-harmonic (base is max amp)
    base = np.argmin(fs)
    baseAmp = np.argmax(amps)
    if base != baseAmp:
        passed = False
        conds[0] = False
        
    # Check IF non-negative
    if np.any(IF[100:-100]<0):
        passed = False
        conds[0] = False
        
    color = 'green' if passed else 'red'
    
    if light:
        # Set card headers with individual conditions
        phase_light = headers[2] + ' 🟢'
        if conds[0]:
            amp_light = headers[0] + ' 🟢'
            cond2_color = 'green'
        else:
            amp_light = headers[0] + ' 🔴'
            cond2_color = 'red'
        if conds[1]:
            freq_light = headers[1] + ' 🟢'
            cond1_color = 'green'
        else: 
            freq_light = headers[1] + ' 🔴'
            cond1_color = 'red'
        # Prepare 1/f^gamma fit
        y_peaks = amps[amps>0]
        f_peaks = fs[amps>0]
        slope2, intercept2, r2, p2, se2 = stats.linregress(np.log10(f_peaks), np.log10(y_peaks))
        freq = np.linspace(0.1, 6, 60)
        ypred_oof = 10**(intercept2 + slope2*np.log10(freq))
        colors = [cond1_color, cond2_color]

    else:
        amp_light = headers[0]
        freq_light = headers[1]
        phase_light = headers[2]
           
    ## Plotting
    fig = px.line(df, x="t", y="y", title="Signal") 
    fig.update_yaxes(range=[-2, 2])
    fig.update_xaxes(title='Time [s]', range=[0, 3])
    fig.update_layout(showlegend=False, font=dict(size=16))
    
    fig2 = px.line(df, x="t", y="IF", title="Instantaneous Frequency") 
    fig2.add_scatter(x=df['t'], y=df['zeros'], name='')
    fig2.update_yaxes(range=[-0.5, IFmax*1.25], title='IF [Hz]')
    fig2.update_xaxes(title='Time [s]', range=[0, 3])
    fig2.update_layout(showlegend=False, font=dict(size=16))
    
    fig3 = px.line(df2, x="xf", y="yf", title="Spectrum") 

    if light:
        fig3.add_scatter(x=freq, y=ypred_oof, name=str(slope2), marker=dict(color='black'))
        fig3.add_annotation(x=3.75, y=ypred_oof[30]+0.1,
            text='1/f^(' + str(np.round(np.abs(slope2), 2)) + ') fit',
            showarrow=False,
            yshift=10, font=dict(size=24))
        fig3.add_scatter(x=[3, 4.5], y=[0.8, 0.8], text=['Condition 1 met', 'Condition 2 met'], 
                     mode="markers+text", textposition="top center",
                     marker=dict(color=colors, size=50))
    else:
        fig3.add_scatter(x=[4], y=[0.8], text='Harmonic conditions met', 
                     mode="markers+text", textposition="top center",
                     marker=dict(color=color, size=50))
    
    fig3.update_xaxes(title='Frequency [Hz]', range=[0, 5.5])
    fig3.update_yaxes(title='Normalised FFT', range=[-0.05, 1.05])
    fig3.update_layout(showlegend=False, font=dict(size=16))
         
    return [fig, fig2, fig3, amp_light, freq_light, phase_light]


if __name__ == '__main__':
    app.run_server(host='127.0.0.1', debug=True)