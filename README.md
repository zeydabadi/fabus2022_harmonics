# Fabus2022_Harmonics

Scripts to reproduce figures from [Fabus et al. (2022)](https://www.biorxiv.org/content/10.1101/2021.12.21.473676v1). Additionally, we include a version of our [shape generator website](shapegen.herokuapp.com/) to be used locally (run the app.py code and go to [127.0.0.1:8050/](127.0.0.1:8050/) in your browser), as well as a Jupyter notebok guiding the reader through some of the main points of the manuscript.

This repo is a work in progress!

